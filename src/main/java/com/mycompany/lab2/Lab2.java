/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author test1
 */
public class Lab2 {
    
    static char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}} ;
    
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int count = 0 ;
        
        printWelcome();
        printTable();
        while(true){
            turnO();
            printTable();
            if (checkO() == true){ System.out.println("O win!"); break; }
            count++ ;
            if (count == 9) {draw(); break;}
            turnX();
            printTable();
            if (checkX() == true){ System.out.println("X win!"); break ;}
            count++ ;
            if (count == 9) {draw(); break;}
            
        }

    }

    private static void printWelcome() {
        System.out.println("Welcome to OX game");
    }

    private static void printTable() {
        for (int i = 0 ; i < 3 ; i++){
            for (int j = 0 ; j < 3 ; j++){
                System.out.print(table[i][j]);
            }
            System.out.println();
        }
            
    }

    private static void turnO() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Turn O input row,column : ");
        int row = kb.nextInt();
        int col = kb.nextInt();
        table[row-1][col-1] = 'O' ;
    }

    private static void turnX() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Turn O input row,column : ");
        int row = kb.nextInt();
        int col = kb.nextInt();
        table[row-1][col-1] = 'X' ;
    }

    private static boolean checkO() {
        for (int i = 0 ; i < 3 ; i++){
            if (table[i][0] == 'O' && table[i][1] == 'O' && table[i][2] == 'O'){
                return true ;
            } else if (table[0][i] == 'O' && table[1][i] == 'O' && table[2][i] == 'O'){
                return true ;
            } else if (table[0][0] == 'O' && table[1][1] == 'O' && table[2][2] == 'O'){
                return true ;
            } else if (table[0][2] == 'O' && table[1][1] == 'O' && table[2][0] == 'O'){
                return true ;
            }   
        }
        return false ;
    }

    private static boolean checkX() {
        for (int i = 0 ; i < 3 ; i++){
            if (table[i][0] == 'X' && table[i][1] == 'X' && table[i][2] == 'X'){
                return true ;
            } else if (table[0][i] == 'X' && table[1][i] == 'X' && table[2][i] == 'X'){
                return true ;
            } else if (table[0][0] == 'X' && table[1][1] == 'X' && table[2][2] == 'X'){
                return true ;
            } else if (table[0][2] == 'X' && table[1][1] == 'X' && table[2][0] == 'X'){
                return true ;
            }   
        }
        return false ;
    }

    private static void draw() {
        System.out.println("Draw!");
    }
}
